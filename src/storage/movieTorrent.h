#ifndef BVD_MOVIETORRENT_H
#define BVD_MOVIETORRENT_H

#include <string>
#include <vector>
#include "torrentConfig.h"

namespace bvd
{
	const std::vector<std::string> videoWhitelist = { ".mp4", ".mkv", ".avi" };
	const std::vector<std::string> subtitlesWhitelist = { ".srt" };

	
	class MovieTorrent : public TorrentConfig
	{
	public:
		MovieTorrent(const std::string& torrentPath = "",
					 const std::string& videoPath = "",
					 const std::string& subtitlesPath = "");

		const std::string& getVideoPath() const { return videoPath; }
		const std::string& getSubtitlesPath() const { return subtitlesPath; }
		
		void setVideoPath(const std::string& path) { videoPath = path; }
		void setSubtitlesPath(const std::string& path) { subtitlesPath = path; }
		
	private:
		std::string videoPath;
		std::string subtitlesPath;

		
	};
}

#endif
