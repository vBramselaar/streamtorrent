#include "storageManager.h"

bvd::StorageManager::StorageManager()
{
	
}

void bvd::StorageManager::addTorrent(const bvd::MovieTorrent &newTorrent)
{
	torrents.push_back(newTorrent);
}

bool bvd::StorageManager::removeTorrent(size_t index)
{
	if (index < torrents.size())
	{
		torrents.erase(torrents.begin() + index);
		return true;
	}

	return false;
}
