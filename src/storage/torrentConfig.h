#ifndef BVD_TORRENTCONFIG_H
#define BVD_TORRENTCONFIG_H

#include <string>

namespace bvd
{
	class TorrentConfig
	{
	public:
		TorrentConfig(const std::string& torrentPath = "");

		const std::string& getTorrentPath() const { return torrentPath; }
		const std::string& getTorrentName() const { return torrentName; }
		void setTorrentPath(const std::string& path) { torrentPath = path; }
		void setTorrentName(const std::string& name) { torrentName = name; }
		
	private:
		std::string torrentPath;
		std::string torrentName;
	};
	
}

#endif
