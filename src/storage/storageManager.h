#ifndef BVD_STORAGEMANAGER_H
#define BVD_STORAGEMANAGER_H

#include <string>
#include <vector>

#include "movieTorrent.h"

namespace bvd
{

	using TorrentList = std::vector<bvd::MovieTorrent>;
	
	class StorageManager
	{
	public:
		StorageManager();
		void addTorrent(const bvd::MovieTorrent& newTorrent);
		bool removeTorrent(size_t index);
		const bvd::MovieTorrent& getTorrent(size_t index) const { return torrents.at(index); }
		const std::vector<bvd::MovieTorrent>& getTorrents() const { return torrents; };

	private:
		std::vector<bvd::MovieTorrent> torrents;
	};
}

#endif
