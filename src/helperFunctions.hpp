#ifndef BVD_HELPERFUNCTIONS_H
#define BVD_HELPERFUNCTIONS_H

#include <libtorrent/torrent_status.hpp>

namespace bvd
{
	inline char const* state(lt::torrent_status::state_t s)
	{
		switch(s) {
		case lt::torrent_status::checking_files:
			return "checking";
		case lt::torrent_status::downloading_metadata:
			return "dl metadata";
		case lt::torrent_status::downloading:
			return "downloading";
		case lt::torrent_status::finished:
			return "finished";
		case lt::torrent_status::seeding:
			return "seeding";
		case lt::torrent_status::checking_resume_data:
			return "checking resume";
		default: return "<>";
		}
	}
}
	
#endif
