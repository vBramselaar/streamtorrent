#ifndef BVD_METADATAWORKER_H
#define BVD_METADATAWORKER_H

#include <QObject>
#include <memory>
#include <libtorrent/session.hpp>
#include <libtorrent/session_params.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/magnet_uri.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/torrent_status.hpp>

#include "torrentManager.h"

Q_DECLARE_METATYPE(std::shared_ptr<std::vector<std::string>>);
const int sharedVectorStringTypeID = qRegisterMetaType<std::shared_ptr<std::vector<std::string>>>();

Q_DECLARE_METATYPE(std::shared_ptr<bvd::TorrentManager>);
const int sharedTorrentManagerTypeID = qRegisterMetaType<std::shared_ptr<bvd::TorrentManager>>();

namespace bvd
{
	class MetadataWorker : public QObject
	{
		Q_OBJECT
		
	public slots:
		void fetch(std::shared_ptr<bvd::TorrentManager> torrentManager, QString magnet);
		void cancel();

	signals:
		void metadataReceived(std::shared_ptr<std::vector<std::string>> data);

	private:
		bool running = true;
	};
}

#endif
