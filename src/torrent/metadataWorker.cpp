#include "metadataWorker.h"
#include "logger.h"

void bvd::MetadataWorker::fetch(std::shared_ptr<bvd::TorrentManager> torrentManager, QString magnet)
{
	std::shared_ptr<std::vector<std::string>> output = std::make_shared<std::vector<std::string>>();
	running = true;

	try
	{
		lt::add_torrent_params atp = lt::parse_magnet_uri(magnet.toStdString());
		atp.flags |= lt::torrent_flags::upload_mode;
		atp.save_path = ".";
	
		torrentManager->ses.async_add_torrent(std::move(atp));
	}
	catch (const std::exception& e)
	{
		output->push_back("BAD");
		output->push_back(e.what());
		ERROR(e.what());
		emit metadataReceived(output);
		return;
	}

	lt::torrent_handle currentHandle;
	
	while(running)
	{
		std::vector<lt::alert*> alerts;
		torrentManager->ses.pop_alerts(&alerts);

		for (lt::alert const* a : alerts)
		{
			if (auto at = lt::alert_cast<lt::add_torrent_alert>(a))
			{
				currentHandle = at->handle;
				LOG("Torrent Added");
			}

			if (lt::alert_cast<lt::metadata_received_alert>(a))
			{
				
				std::shared_ptr<const lt::torrent_info> info = currentHandle.torrent_file();
				const lt::file_storage& files = info->files();
				for (const auto i : files.file_range())
				{
					output->push_back(files.file_path(i));
				}

				emit metadataReceived(output);
				LOG("Metadata received");
				running = false;
			}

			if (lt::alert_cast<lt::metadata_failed_alert>(a))
			{
				output->push_back("BAD");
				output->push_back(a->message());
				
				emit metadataReceived(output);
				LOG("Metadata failed");
				running = false;;
			}
		}
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}

	torrentManager->ses.remove_torrent(currentHandle);
}

void bvd::MetadataWorker::cancel()
{
	running = false;
}
