#include "torrentManager.h"
#include "logger.h"

bvd::TorrentManager::TorrentManager(QObject *parent) : QObject(parent)
{
	lt::settings_pack pack;
	pack.set_int(lt::settings_pack::alert_mask
				 , lt::alert_category::error
				 | lt::alert_category::storage
				 | lt::alert_category::status);

	ses.apply_settings(pack);
}

bvd::TorrentManager::~TorrentManager()
{
	stopDownload();
}

bool bvd::TorrentManager::stopDownload()
{
	if (currentHandle.is_valid())
	{
		ses.remove_torrent(currentHandle);
	}
	return true;
}

void bvd::TorrentManager::playTorrent()
{
	if (!currentHandle.is_valid())
	{
		return;
	}

	std::vector<lt::download_priority_t> const pieces(currentHandle.torrent_file()->num_pieces(), lt::default_priority);
	currentHandle.prioritize_pieces(pieces);

	currentHandle.resume();

	while(true)
	{
		std::vector<lt::alert*> alerts;
		ses.pop_alerts(&alerts);

		for (lt::alert const* a : alerts)
		{
			if (lt::alert_cast<lt::torrent_finished_alert>(a))
			{
				LOG("finished");
			}
			
			if (lt::alert_cast<lt::torrent_error_alert>(a))
			{
				ERROR(a->message());
			}

			if (lt::alert_cast<lt::torrent_paused_alert>(a))
			{
				LOG("pause alert");
			}

			if (auto st = lt::alert_cast<lt::state_update_alert>(a))
			{
				if (!st->status.empty())
				{
					lt::torrent_status const& s = st->status[0];
					LOG(state(s.state) << ' '
						<< (s.download_payload_rate / 1000) << " kB/s "
						<< (s.total_done / 1000) << " kB ("
						<< (s.progress_ppm / 10000) << "%) downloaded ("
						<< s.num_peers << " peers)\x1b[K");
				}
			}
		}

		if (currentHandle.status().finished_duration >= lt::seconds(5))
		{
			LOG("finished timeout");
			break;
		}
		
		ses.post_torrent_updates();
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
}

void bvd::TorrentManager::asyncPlayTorrent()
{
	playFuture = QtConcurrent::run(this, &TorrentManager::playTorrent);
}


void bvd::TorrentManager::setCurrentHandle(lt::torrent_handle newHandle)
{
	if (currentHandle.is_valid())
	{
		ses.remove_torrent(currentHandle);
	}

	currentHandle = newHandle;
}
