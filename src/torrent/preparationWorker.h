#ifndef BVD_PREPARATIONWORKER_H
#define BVD_PREPARATIONWORKER_H

#include <QObject>
#include "torrentManager.h"
#include "storage/movieTorrent.h"
#include "helperFunctions.hpp"

#include <libtorrent/session.hpp>
#include <libtorrent/session_params.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/torrent_status.hpp>
#include <libtorrent/read_resume_data.hpp>
#include <libtorrent/write_resume_data.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/magnet_uri.hpp>

Q_DECLARE_METATYPE(bvd::MovieTorrent);
const int movieTorrentTypeID = qRegisterMetaType<bvd::MovieTorrent>();

namespace bvd
{
	class PreparationWorker : public QObject
	{
		Q_OBJECT
		
	public slots:
		void prepare(std::shared_ptr<bvd::TorrentManager> torrentManager,
					 bvd::MovieTorrent movieTorrent);
		
		void cancel();
		
	signals:
		void preparationComplete(bool result);
		
	private:
		bool running = false;

		void setPriorities(const lt::torrent_handle& currentHandle,
						   const bvd::MovieTorrent& movieTorrent);
	};
}

#endif
