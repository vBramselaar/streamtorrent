#include "preparationWorker.h"
#include "logger.h"

void bvd::PreparationWorker::prepare(std::shared_ptr<bvd::TorrentManager> torrentManager, bvd::MovieTorrent movieTorrent)
{
	try
	{
		lt::add_torrent_params atp = lt::parse_magnet_uri(movieTorrent.getTorrentPath());
		atp.save_path = ".";
		atp.flags &= ~lt::torrent_flags::auto_managed;
		atp.flags |= lt::torrent_flags::upload_mode;
		atp.flags |= lt::torrent_flags::sequential_download;
		
		torrentManager->ses.async_add_torrent(std::move(atp));
	}
	catch (const std::exception& e)
	{
		ERROR(e.what());
		emit preparationComplete(false);
		return;
	}

	lt::torrent_handle currentHandle;

	running = true;
	while(running)
	{
		std::vector<lt::alert*> alerts;
		torrentManager->ses.pop_alerts(&alerts);

		for (lt::alert const* a : alerts)
		{
			if (auto at = lt::alert_cast<lt::add_torrent_alert>(a))
			{
				currentHandle = at->handle;
				currentHandle.resume();
				LOG("torrent added");
			}

			if (lt::alert_cast<lt::metadata_received_alert>(a))
			{
				LOG("Metadata received");
				setPriorities(currentHandle, movieTorrent);
				currentHandle.unset_flags(lt::torrent_flags::upload_mode);
			}

			if (lt::alert_cast<lt::torrent_finished_alert>(a))
			{
				LOG("finished");
			}
			
			if (lt::alert_cast<lt::torrent_error_alert>(a))
			{
				ERROR(a->message());
				emit preparationComplete(false);
				running = false;
			}

			if (auto st = lt::alert_cast<lt::state_update_alert>(a))
			{
				if (!st->status.empty())
				{
					lt::torrent_status const& s = st->status[0];
					LOG(state(s.state) << ' '
						<< (s.download_payload_rate / 1000) << " kB/s "
						<< (s.total_done / 1000) << " kB ("
						<< (s.progress_ppm / 10000) << "%) downloaded ("
						<< s.num_peers << " peers)\x1b[K");
				}
			}
		}

		if (currentHandle.is_valid() && currentHandle.status().finished_duration >= lt::seconds(5))
		{
			LOG("finished timeout");
			torrentManager->setCurrentHandle(currentHandle);
			emit preparationComplete(true);
			running = false;
		}
		
		torrentManager->ses.post_torrent_updates();
		std::this_thread::sleep_for(std::chrono::milliseconds(200));
	}
}

void bvd::PreparationWorker::cancel()
{
	running = false;
}

void bvd::PreparationWorker::setPriorities(const lt::torrent_handle& currentHandle, const bvd::MovieTorrent& movieTorrent)
{
	size_t videoIndex = 0;
	size_t subIndex = 0;
	
	std::shared_ptr<const lt::torrent_info> info = currentHandle.torrent_file();
	const lt::file_storage& files = info->files();

	std::string video = movieTorrent.getTorrentName() + "/" + movieTorrent.getVideoPath();
	std::string subtitles = movieTorrent.getTorrentName() + "/" + movieTorrent.getSubtitlesPath();
	
	for (size_t i = 0; i < files.num_files(); i++)
	{
		if (files.file_path(i) == video)
		{
			videoIndex = i;
		}
		else if (files.file_path(i) == subtitles)
		{
			subIndex = i;
		}
	}
	
	std::vector<lt::download_priority_t> filePrior(files.num_files(), lt::dont_download);
	filePrior[videoIndex] = lt::default_priority;
	filePrior[subIndex] = lt::default_priority;
	currentHandle.prioritize_files(filePrior);

	std::vector<lt::download_priority_t> const pieces(info->num_pieces(), lt::dont_download);
	currentHandle.prioritize_pieces(pieces);

	//vid piecess
	lt::piece_index_t vidFirstPiece = info->map_file(videoIndex, 0, 0).piece;
	lt::piece_index_t vidLastPiece = info->map_file(videoIndex, info->files().file_size(videoIndex) - 1, 0).piece;

	//sub pieces
	lt::piece_index_t subFirstPiece = info->map_file(subIndex, 0, 0).piece;
	lt::piece_index_t subLastPiece = info->map_file(subIndex, info->files().file_size(subIndex) - 1, 0).piece;

	std::int64_t fileSize = info->files().file_size(videoIndex);
	int percentage = std::ceil((vidLastPiece - vidFirstPiece) * 0.01);
	
	LOG("Torrent Pieces: " << info->num_pieces());
	LOG("FileSise: " << fileSize);
	LOG("First piece: " << vidFirstPiece);
	LOG("Last piece: " << vidLastPiece);
	LOG("1% pieces: " << percentage);

	for (int i = 0; i < percentage; i++)
	{
		currentHandle.piece_priority(vidFirstPiece + static_cast<lt::piece_index_t>(i), lt::default_priority);
		currentHandle.piece_priority(static_cast<long>(vidLastPiece) - i, lt::default_priority);

		currentHandle.piece_priority(subFirstPiece + static_cast<lt::piece_index_t>(i), lt::default_priority);
		currentHandle.piece_priority(static_cast<long>(subLastPiece) - i, lt::default_priority);
	}
}
