#ifndef BVD_TORRENTMANAGER_H
#define BVD_TORRENTMANAGER_H

#include <vector>
#include <string>
#include <memory>
#include <thread>
#include <chrono>
#include <cmath>
#include <system_error>

#include <QObject>
#include <QtConcurrent>
#include <QThread>

#include <libtorrent/session.hpp>
#include <libtorrent/session_params.hpp>
#include <libtorrent/add_torrent_params.hpp>
#include <libtorrent/torrent_handle.hpp>
#include <libtorrent/alert_types.hpp>
#include <libtorrent/bencode.hpp>
#include <libtorrent/torrent_status.hpp>
#include <libtorrent/read_resume_data.hpp>
#include <libtorrent/write_resume_data.hpp>
#include <libtorrent/error_code.hpp>
#include <libtorrent/magnet_uri.hpp>

#include "storage/movieTorrent.h"
#include "helperFunctions.hpp"

namespace bvd
{
	class TorrentManager : public QObject
	{
		Q_OBJECT
		friend class MetadataWorker;
		friend class PreparationWorker;
		
	public:
		explicit TorrentManager(QObject *parent = nullptr);
		~TorrentManager();

		bool stopDownload();
		void playTorrent();
		void asyncPlayTorrent();
		void setCurrentHandle(lt::torrent_handle newHandle);


	signals:
		void torrentAdded();
		
	private:
		lt::session ses;
		lt::torrent_handle currentHandle;
		QFuture<void> playFuture;
	};
}

#endif
