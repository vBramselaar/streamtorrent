#ifndef ADDTORRENTDIALOG_H
#define ADDTORRENTDIALOG_H

#include <QDialog>
#include <QMovie>
#include <QString>
#include <QThread>
#include <string>
#include <algorithm>
#include <memory>
#include "torrent/torrentManager.h"
#include "torrent/metadataWorker.h"
#include "storage/movieTorrent.h"
#include "storage/storageManager.h"

namespace Ui {
	class AddTorrentDialog;
}

class AddTorrentDialog : public QDialog
{
    Q_OBJECT

public:
    explicit AddTorrentDialog(std::shared_ptr<bvd::TorrentManager> torrentManager,
							  bvd::StorageManager& storageManager,
							  QWidget *parent = nullptr);
    ~AddTorrentDialog();

private slots:
	void fetchButtonPressed();
	void metadataReceived(std::shared_ptr<std::vector<std::string>> output);
	void saveButtonClicked();

signals:
	void doFetchMetadata(std::shared_ptr<bvd::TorrentManager> torrentManager, QString magnet);
	
private:
    Ui::AddTorrentDialog *ui;
	std::shared_ptr<bvd::TorrentManager> torrentManager;
	bvd::StorageManager& storageManager;
	QMovie* loadingIcon;
	bvd::MovieTorrent newMovieTorrent;

	QThread metadataThread;
	bvd::MetadataWorker* metadataWorker;
};

#endif // ADDTORRENTDIALOG_H
