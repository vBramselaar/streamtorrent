#include "addtorrentdialog.h"
#include "ui_addtorrentdialog.h"
#include "logger.h"

AddTorrentDialog::AddTorrentDialog(std::shared_ptr<bvd::TorrentManager> torrentManager,
								   bvd::StorageManager& storageManager,
								   QWidget *parent) :
	torrentManager(torrentManager),
	storageManager(storageManager),
    QDialog(parent),
    ui(new Ui::AddTorrentDialog)
{
    ui->setupUi(this);

	//Creative common movie from the Blender foundation
	const QString SINTEL = "magnet:?xt=urn:btih:08ada5a7a6183aae1e09d831df6748d566095a10&dn=Sintel&tr=udp%3A%2F%2Fexplodie.org%3A6969&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.empire-js.us%3A1337&tr=udp%3A%2F%2Ftracker.leechers-paradise.org%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337&tr=wss%3A%2F%2Ftracker.btorrent.xyz&tr=wss%3A%2F%2Ftracker.fastcast.nz&tr=wss%3A%2F%2Ftracker.openwebtorrent.com&ws=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2F&xs=https%3A%2F%2Fwebtorrent.io%2Ftorrents%2Fsintel.torrent";

	//test placeholder
	ui->lineEdit_magnetLink->setText(SINTEL);


	loadingIcon = new QMovie("./resources/loading.gif");
	if (!loadingIcon->isValid())
	{
		this->close();
		return;
	}
	ui->label_fetchingIcon->setMovie(loadingIcon);
	ui->label_fetchError->setStyleSheet("QLabel { color : red; }");
	ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);

	metadataWorker = new bvd::MetadataWorker;
	metadataWorker->moveToThread(&metadataThread);
	connect(&metadataThread, &QThread::finished, metadataWorker, &QObject::deleteLater);
	connect(this, &AddTorrentDialog::doFetchMetadata, metadataWorker, &bvd::MetadataWorker::fetch);
	connect(metadataWorker, &bvd::MetadataWorker::metadataReceived, this, &AddTorrentDialog::metadataReceived);
	metadataThread.start();

	connect(ui->pushButton_fetchMetadata, &QAbstractButton::clicked, this, &AddTorrentDialog::fetchButtonPressed);
	connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &QWidget::close);
	connect(ui->buttonBox, &QDialogButtonBox::accepted, this, &AddTorrentDialog::saveButtonClicked);
}

AddTorrentDialog::~AddTorrentDialog()
{
	metadataWorker->cancel();
	metadataThread.quit();
	metadataThread.wait();
	
    delete ui;
	delete loadingIcon;
}

void AddTorrentDialog::fetchButtonPressed()
{
	if (ui->lineEdit_magnetLink->text().isEmpty())
	{
		return;
	}

	ui->pushButton_fetchMetadata->setEnabled(false);
	ui->label_fetchingIcon->setMovie(loadingIcon);
	loadingIcon->start();

	emit doFetchMetadata(torrentManager, ui->lineEdit_magnetLink->text());
}

void AddTorrentDialog::metadataReceived(std::shared_ptr<std::vector<std::string>> output)
{
	loadingIcon->stop();
	ui->label_fetchingIcon->clear();

	if (output->at(0) == "BAD")
	{
		ui->label_fetchError->setText(QString::fromStdString(output->at(1)));
		return;
	}

	ui->groupBox_result->setEnabled(true);

	ui->listWidget_filesOverview->clear();
	ui->comboBox_videoChoice->clear();
	ui->comboBox_subtitlesChoice->clear();

	const std::string& name = output->at(0);
	ui->label_torrentName->setText(QString::fromStdString(name.substr(0, name.find('/'))));

	for (const std::string& file : *output)
	{
		ui->listWidget_filesOverview->addItem(QString(file.c_str()));
		
		auto itVid = std::find (bvd::videoWhitelist.begin(), bvd::videoWhitelist.end(), file.substr(file.find_last_of(".")));
		if (itVid != bvd::videoWhitelist.end())
		{
			ui->comboBox_videoChoice->addItem(QString::fromStdString(file.substr(file.find('/') + 1)));
		}
		
		auto itSub = std::find (bvd::subtitlesWhitelist.begin(), bvd::subtitlesWhitelist.end(), file.substr(file.find_last_of(".")));
		if (itSub != bvd::subtitlesWhitelist.end())
		{
			ui->comboBox_subtitlesChoice->addItem(QString::fromStdString(file.substr(file.find('/') + 1)));
		}
	}

	ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(true);
	ui->pushButton_fetchMetadata->setEnabled(true);
}


void AddTorrentDialog::saveButtonClicked()
{
	bvd::MovieTorrent newTorrent;

	newTorrent.setTorrentPath(ui->lineEdit_magnetLink->text().toStdString());
	newTorrent.setTorrentName(ui->label_torrentName->text().toStdString());
	newTorrent.setVideoPath(ui->comboBox_videoChoice->currentText().toStdString());
	newTorrent.setSubtitlesPath(ui->comboBox_subtitlesChoice->currentText().toStdString());

	storageManager.addTorrent(newTorrent);

	ui->buttonBox->button(QDialogButtonBox::Save)->setEnabled(false);
}
