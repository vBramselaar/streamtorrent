#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QString>
#include <QFileDialog>
#include <QListWidget>
#include <QThread>

#include <iostream>
#include <string>
#include <vector>
#include <memory>
#include <sstream>

#include "forms/addtorrentdialog.h"
#include "torrent/torrentManager.h"
#include "torrent/preparationWorker.h"
#include "storage/storageManager.h"
#include "mpv/mpvwidget.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QWidget
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
	void playPressed();
	void buttonBackPressed();
	void addNewTorrentPressed();
	void torrentListClicked();
	void preparationDone(bool result);

signals:
	void doPrepareTorrent(std::shared_ptr<bvd::TorrentManager> torrentManager,
						  bvd::MovieTorrent movieTorrent);

private:
    Ui::MainWindow *ui;
	MpvWidget* m_mpv;
	std::shared_ptr<bvd::TorrentManager> torrentManager;
	bvd::StorageManager storageManager;

	QThread preparationThread;
	bvd::PreparationWorker* preparationWorker;
};

#endif // MAINWINDOW_H
