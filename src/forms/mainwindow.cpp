#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
	QWidget(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	torrentManager = std::make_shared<bvd::TorrentManager>();
	m_mpv = ui->videoPlayer;


	const bvd::TorrentList& torrentList = storageManager.getTorrents();
	for (const bvd::MovieTorrent& torrent : torrentList)
	{
		ui->listWidget->addItem(QString::fromStdString(torrent.getVideoPath()));
	}

	ui->pushButton_playTorrent->setEnabled(false);

	preparationWorker = new bvd::PreparationWorker;
	preparationWorker->moveToThread(&preparationThread);
	connect(&preparationThread, &QThread::finished, preparationWorker, &QObject::deleteLater);
	connect(this, &MainWindow::doPrepareTorrent, preparationWorker, &bvd::PreparationWorker::prepare);
	connect(preparationWorker, &bvd::PreparationWorker::preparationComplete, this, &MainWindow::preparationDone);
	preparationThread.start();
	
	
	connect(ui->pushButton_playTorrent, &QAbstractButton::clicked, this, &MainWindow::playPressed);
	connect(ui->pushButton_back, &QAbstractButton::clicked, this, &MainWindow::buttonBackPressed);
	connect(ui->toolButton_add_torrent, &QAbstractButton::clicked, this, &MainWindow::addNewTorrentPressed);
	connect(ui->listWidget, &QListWidget::itemSelectionChanged, this, &MainWindow::torrentListClicked);

}

MainWindow::~MainWindow()
{
	preparationWorker->cancel();
	preparationThread.quit();
	preparationThread.wait();
	delete ui;
}


void MainWindow::addNewTorrentPressed()
{
	AddTorrentDialog dialog(torrentManager, storageManager, this);
    dialog.setModal(true);
    dialog.exec();

	ui->listWidget->clear();

	const bvd::TorrentList& torrentList = storageManager.getTorrents();
	for (const bvd::MovieTorrent& torrent : torrentList)
	{
		ui->listWidget->addItem(QString::fromStdString(torrent.getVideoPath()));
	}
}

void MainWindow::playPressed()
{
	const bvd::MovieTorrent& selectedTorrent = storageManager.getTorrent(ui->listWidget->currentRow());
	emit doPrepareTorrent(torrentManager, selectedTorrent);
	ui->pushButton_playTorrent->setEnabled(false);
	ui->listWidget->setEnabled(false);
}

void MainWindow::buttonBackPressed()
{
	m_mpv->command(std::vector<std::string>{"stop"});
	ui->stackedWidget->setCurrentIndex(1);
	ui->pushButton_playTorrent->setEnabled(true);
	ui->listWidget->setEnabled(true);
}

void MainWindow::torrentListClicked()
{
	QListWidget* listWidget = ui->listWidget;
	if (ui->listWidget->currentRow() >= 0)
	{
		ui->pushButton_playTorrent->setEnabled(true);
		const bvd::MovieTorrent& selectedTorrent = storageManager.getTorrent(listWidget->currentRow());

		ui->label_torrentName->setText(QString::fromStdString(selectedTorrent.getTorrentName()));
		ui->label_videoName->setText(QString::fromStdString(selectedTorrent.getVideoPath()));
		ui->label_subtitleName->setText(QString::fromStdString(selectedTorrent.getSubtitlesPath()));
	}
	else
	{
		ui->pushButton_playTorrent->setEnabled(false);
	}
}

void MainWindow::preparationDone(bool result)
{
	if (result)
	{
		ui->stackedWidget->setCurrentIndex(0);
		torrentManager->asyncPlayTorrent();

		const bvd::MovieTorrent& selectedTorrent = storageManager.getTorrent(ui->listWidget->currentRow());
		m_mpv->setProperty("sub-files", QString::fromStdString(selectedTorrent.getTorrentName() + "/" + selectedTorrent.getSubtitlesPath()));
		m_mpv->command(std::vector<std::string>{"loadfile", selectedTorrent.getTorrentName() + "/" + selectedTorrent.getVideoPath()});
	}
	else
	{
		ui->pushButton_playTorrent->setEnabled(true);
		ui->listWidget->setEnabled(true);
	}
	
}
