#ifndef PLAYERWINDOW_H
#define PLAYERWINDOW_H

#include <QtWidgets/QOpenGLWidget>
#include <mpv/client.h>
#include <mpv/render_gl.h>

class MpvWidget : public QOpenGLWidget
{
    Q_OBJECT
	
public:
    MpvWidget(QWidget* parent = 0, Qt::WindowFlags f = 0);
    ~MpvWidget();
    void command(const std::vector<std::string>& params);
    void setProperty(const QString& name, const QString& value);
    QVariant getProperty(const QString& name) const;
    QSize sizeHint() const { return QSize(480, 270); }
	
signals:
    void durationChanged(int value);
    void positionChanged(int value);
	
protected:
    void initializeGL() override;
    void paintGL() override;
								  
private slots:
    void on_mpv_events();
    void maybeUpdate();
	
private:
    void handle_mpv_event(mpv_event* event);
    static void on_update(void* ctx);

    mpv_handle* mpv;
    mpv_render_context* mpv_gl;
};



#endif // PLAYERWINDOW_H
