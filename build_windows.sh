#!/bin/bash

set -e
set -x

# third-party
rm -rf "./third-party/build"

cmake -S "./third-party" -B "./third-party/build" -G "MSYS Makefiles"
cmake --build "./third-party/build"

# main-app
rm -rf "./build"

cmake -S . -B "./build" -G "MSYS Makefiles" -DCMAKE_INSTALL_PREFIX="./output"
cmake --build "./build" --target install

# copy dlls (currently implemented in cmake)
#cp $(find $(ldd ./output/streamTorrent.exe | grep -Po '/.*(?= \(0x)') -path "/mingw64*") ./output
#windeployqt.exe ./output/streamTorrent.exe
