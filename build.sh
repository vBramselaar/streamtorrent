#!/bin/bash

set -e
set -x

# third-party
rm -rf "./third-party/build"

cmake -S "./third-party" -B "./third-party/build"
cmake --build "./third-party/build"

# main-app
rm -rf "./build"

cmake -S . -B "./build"
cmake --build "./build" --target install
