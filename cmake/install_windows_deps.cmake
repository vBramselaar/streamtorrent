message(STATUS "Finding dll files for ${EXE}")
file(GET_RUNTIME_DEPENDENCIES
    EXECUTABLES "${EXE}"
    RESOLVED_DEPENDENCIES_VAR found_deps
	UNRESOLVED_DEPENDENCIES_VAR unfound_deps
	DIRECTORIES "C:/msys64/mingw64/bin"
    )
list(FILTER found_deps INCLUDE REGEX "^C:/msys64")

message(STATUS "Copying dll files to ${OUTPUT}")
file(INSTALL ${found_deps} DESTINATION "${OUTPUT}")