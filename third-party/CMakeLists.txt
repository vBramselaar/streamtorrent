cmake_minimum_required(VERSION 3.13.4)
project(streamTorrent_third-party)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(LOCAL_BOOST OFF CACHE BOOL "If you want to use local build Boost")


set(LIBTORRENT_PATCH echo "No patching needed")

if(${CMAKE_VERSION} VERSION_LESS "3.15.0")
  set(LIBTORRENT_PATCH patch -f -r - <SOURCE_DIR>/CMakeLists.txt ${PROJECT_SOURCE_DIR}/debianBuster.patch || true)
endif()

include(ExternalProject)

if(LOCAL_BOOST)
  ExternalProject_Add(boost
	URL                    "https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_69_0.7z"
	                       "https://dl.bintray.com/boostorg/release/1.70.0/source/boost_1_69_0.zip"
	SOURCE_DIR             "${PROJECT_SOURCE_DIR}/boost-src"
	INSTALL_DIR            "${PROJECT_SOURCE_DIR}/boost"
	BUILD_IN_SOURCE        ON
	CONFIGURE_COMMAND      ./bootstrap.sh --prefix=<INSTALL_DIR>
	BUILD_COMMAND          ./b2 install address-model=64 architecture=x86 link=static variant=release threading=multi runtime-link=static
	INSTALL_COMMAND        ""
	)

  list(APPEND BOOST_FLAGS
	"-DBOOST_ROOT=${PROJECT_SOURCE_DIR}/boost"
	"-DBOOST_INCLUDEDIR=${PROJECT_SOURCE_DIR}/boost/include"
	"-DBOOST_LIBRARYDIR=${PROJECT_SOURCE_DIR}/boost/lib"
	"-DBoost_NO_SYSTEM_PATHS=ON"
	)
endif()



ExternalProject_Add(libtorrent
  GIT_REPOSITORY         "https://github.com/arvidn/libtorrent.git"
  GIT_TAG                "v2.0.2"
  GIT_PROGRESS           True
  GIT_SHALLOW            True
  GIT_SUBMODULES_RECURSE True
  SOURCE_DIR             "${PROJECT_SOURCE_DIR}/libtorrent-src"
  BINARY_DIR             "${PROJECT_SOURCE_DIR}/libtorrent-build"
  INSTALL_DIR            "${PROJECT_SOURCE_DIR}/libtorrent"
  CMAKE_ARGS             "-DBUILD_SHARED_LIBS=OFF" "-DCMAKE_BUILD_TYPE=Release" "-DCMAKE_CXX_STANDARD=14" "-DCMAKE_INSTALL_PREFIX=<INSTALL_DIR>" ${BOOST_FLAGS}
  PATCH_COMMAND          "${LIBTORRENT_PATCH}"
  )
